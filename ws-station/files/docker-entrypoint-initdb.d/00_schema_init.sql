create schema stationrequest;
create table networks(
  network_id BIGINT DEFAULT 0 PRIMARY KEY,
  network TEXT,
  start_year INTEGER,
  end_year INTEGER,
  description TEXT,
  starttime TIMESTAMP WITHOUT TIME ZONE DEFAULT '-infinity',
  endtime TIMESTAMP WITHOUT TIME ZONE DEFAULT 'infinity',
  policy TEXT,
  altcode TEXT,
  histcode TEXT,
  nb_station INTEGER
  );
create table response(
  response_id bigint primary key,
  source_file text,
  xml text,
  channel_id bigint
);

create table ws_common (
  channel_id bigint,
  network_id bigint,
  station_id bigint,
  network text,
  station text,
  location text,
  channel text,
  start_year integer,
  end_year integer,
  policy text,
  latitude float,
  longitude float,
  nstarttime timestamp with time zone,
  nendtime timestamp with time zone,
  sstarttime timestamp with time zone,
  sendtime timestamp with time zone,
  cstarttime timestamp with time zone,
  cendtime timestamp with time zone,
  mtc boolean,
  mts boolean,
  updated timestamp with time zone
);

CREATE INDEX ix_ws_1 ON ws_common(network_id);
CREATE INDEX ix_ws_2 ON ws_common(station_id);
CREATE UNIQUE INDEX ix_ws_3 ON ws_common(channel_id);

CREATE INDEX ix_ws_4 ON ws_common(channel);
CREATE INDEX ix_ws_5 ON ws_common(location);
CREATE INDEX ix_ws_6 ON ws_common(network);
CREATE INDEX ix_ws_7 ON ws_common(station);

CREATE INDEX ix_ws_001 ON ws_common(nstarttime);
CREATE INDEX ix_ws_002 ON ws_common(nendtime);
CREATE INDEX ix_ws_003 ON ws_common(nstarttime, nendtime);
CREATE INDEX ix_ws_004 ON ws_common(sstarttime);
CREATE INDEX ix_ws_005 ON ws_common(sendtime);
CREATE INDEX ix_ws_006 ON ws_common(sstarttime, sendtime);
CREATE INDEX ix_ws_007 ON ws_common(cstarttime);
CREATE INDEX ix_ws_008 ON ws_common(cendtime);
CREATE INDEX ix_ws_009 ON ws_common(cstarttime, cendtime);
CREATE INDEX ix_ws_010 ON ws_common(latitude);
CREATE INDEX ix_ws_011 ON ws_common(longitude);
CREATE INDEX ix_ws_012 ON ws_common(longitude,latitude);
CREATE INDEX ix_ws_013 ON ws_common(start_year);
CREATE INDEX ix_ws_014 ON ws_common(end_year);
CREATE INDEX ix_ws_015 ON ws_common(network,start_year, end_year);
CREATE INDEX ix_ws_016 ON ws_common(mtc);
CREATE INDEX ix_ws_019 ON ws_common(mts);
CREATE INDEX ix_ws_017 ON ws_common(policy);
CREATE INDEX ix_ws_018 ON ws_common(updated);
create table ws_network_text (
    channel_id bigint,
    nettext text
    );
create unique index ix_ws_network_text_1 on ws_network_text(channel_id);

create table ws_station_text (
    channel_id bigint,
    statext text
);
CREATE UNIQUE INDEX ix_ws_station_text_1 ON ws_station_text(channel_id);

create table ws_channel_text (
    channel_id bigint,
    chatext text
);
CREATE UNIQUE INDEX ix_ws_channel_text_1 ON ws_channel_text(channel_id);


create table ws_network_xml (
    channel_id bigint,
    xmlnet1 text
    );
create unique index ix_ws_network_xml_1 on ws_network_text(channel_id);

create table ws_station_xml (
    channel_id bigint,
    xmlstat1 text,
    xmlstat11 text,
    xmlstat22 text
);
CREATE UNIQUE INDEX ix_ws_station_xml_1 ON ws_station_text(channel_id);

create table ws_channel_xml (
    channel_id bigint,
    xmlchan1 text,
    xmlchan2 text
);
CREATE UNIQUE INDEX ix_ws_channel_xml_1 ON ws_channel_text(channel_id);
