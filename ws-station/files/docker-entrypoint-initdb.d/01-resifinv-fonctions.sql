CREATE OR REPLACE function public.station_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
staall integer;    -- # of different station code in the request
stastar integer;    -- # of '*' for station in the request
stalike integer;
BEGIN
clause := '  AND w.station LIKE t.station '; -- the most general clause
staall :=  (select count (distinct t.station) FROM temp_req t);
stastar := (select count (distinct t.station) FROM temp_req t where t.station = '*');
stalike :=
   (select count (distinct t.station) from temp_req t where t.station like '%*%') +
   (select count (distinct t.station) from temp_req t where t.station like '*%') +
   (select count (distinct t.station) from temp_req t where t.station like '%*') +
   (select count (distinct t.station) from temp_req t where t.station like '%?%') +
   (select count (distinct t.station) from temp_req t where t.station like '%?') +
   (select count (distinct t.station) from temp_req t where t.station like '?%');
CASE
   -- il n'y a qu'un seul code station et il est égale = 1
   WHEN ((staall = 1) and (stastar = 1)) THEN
      clause := '';
   -- il y a plusieurs codes station et aucun d'entre eux ne contient de 'µ'
   WHEN (stalike = 0)  THEN
      clause := '  AND w.station = t.station ';
   ELSE
      clause := '  AND w.station LIKE t.station ';
END CASE;
return clause;
END; $$
LANGUAGE 'plpgsql';

CREATE OR REPLACE function public.updateafter_clause() RETURNS TEXT AS $$
-- used to format and optimize a ws station-request
DECLARE
clause TEXT;
BEGIN
clause := '';
SELECT ' AND w.updated >  t.updatedafter '
from temp_req
where temp_req.textdate7 != '*'  INTO clause;
return clause;
END; $$
LANGUAGE 'plpgsql';

CREATE OR REPLACE function public.include_restricted_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
BEGIN
clause := '';
-- SELECT '  AND w.open =  TRUE '
SELECT '  AND w.policy =  ''open'' '
from temp_req
where temp_req.includerestricted = FALSE INTO clause;
return clause;
END; $$
LANGUAGE 'plpgsql';

CREATE OR REPLACE function public.network_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
netall integer;    -- # of different network code in the request
netstar integer;    -- # of '*' for network in the request
netlike integer;
BEGIN
clause := ' AND w.network LIKE t.network ';
netall :=  (select count (distinct t.network) from temp_req t);
netstar := (select count (distinct t.network) from temp_req t where t.network = '*');
netlike :=
   (select count (distinct t.network) from temp_req t where t.network like '%*%') +
   (select count (distinct t.network) from temp_req t where t.network like '*%') +
   (select count (distinct t.network) from temp_req t where t.network like '%*') +
   (select count (distinct t.network) from temp_req t where t.network like '%?%') +
   (select count (distinct t.network) from temp_req t where t.network like '%?') +
   (select count (distinct t.network) from temp_req t where t.network like '?%');
CASE
   -- il n'y a qu'un seul code network et il est égal = '*'
   WHEN ((netall = 1) and (netstar = 1)) THEN
      clause := '';
   -- il y a plusieurs codes network et aucun d'entre eux ne contient de joker
   WHEN (netlike = 0)  THEN
--      clause := ' AND t.network = w.network ';
     clause := ' AND w.network = t.network ';
   ELSE
--      clause := ' AND t.network LIKE w.network ';
	clause := ' AND w.network LIKE t.network ';
END CASE;
return clause;
END; $$
LANGUAGE 'plpgsql';

CREATE OR REPLACE function public.location_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
locall integer;    -- # of different station code in the request
locstar integer;    -- # of '*' for station in the request
loclike integer;
BEGIN
clause := '  AND w.location LIKE t.location '; -- the most general clause
locall :=  (select count (distinct t.location) from temp_req t);
locstar := (select count (distinct t.location) from temp_req t where t.location = '*');
loclike :=
   (select count (distinct t.location) from temp_req t where t.location like '%*%') +
   (select count (distinct t.location) from temp_req t where t.location like '*%') +
   (select count (distinct t.location) from temp_req t where t.location like '%*') +
   (select count (distinct t.location) from temp_req t where t.location like '%?%') +
   (select count (distinct t.location) from temp_req t where t.location like '%?') +
   (select count (distinct t.location) from temp_req t where t.location like '?%');
CASE
   -- il n'y a qu'un seul code location et il est égale = 1
   WHEN ((locall = 1) and (locstar = 1)) THEN
      clause := '';
   -- il y a plusieurs codes station et aucun d'entre eux ne contient de 'µ'
   WHEN (loclike = 0)  THEN
      clause := ' AND w.location = t.location ';
   ELSE
      clause := ' AND w.location LIKE t.location ';
END CASE;
return clause;
END; $$
LANGUAGE 'plpgsql';


CREATE OR REPLACE function public.channel_clause() RETURNS TEXT AS $$
DECLARE
clause TEXT;
chaall integer;    -- # of different station code in the request
chastar integer;    -- # of '*' for station in the request
chalike integer;
BEGIN
clause := '  AND w.channel LIKE t.channel '; -- the most general clause
chaall :=  (select count (distinct t.channel) from temp_req t);
chastar := (select count (distinct t.channel) from temp_req t where t.channel = '*');
chalike :=
   (select count (distinct t.channel) from temp_req t where t.channel like '%*%') +
   (select count (distinct t.channel) from temp_req t where t.channel like '*%') +
   (select count (distinct t.channel) from temp_req t where t.channel like '%*') +
   (select count (distinct t.channel) from temp_req t where t.channel like '%?%') +
   (select count (distinct t.channel) from temp_req t where t.channel like '%?') +
   (select count (distinct t.channel) from temp_req t where t.channel like '?%');
CASE
   -- il n'y a qu'un seul code station et il est égale = 1
   WHEN ((chaall = 1) and (chastar = 1)) THEN
      clause := '';
   -- il y a plusieurs codes station et aucun d'entre eux ne contient de 'µ'
   WHEN (chalike = 0)  THEN
      clause := '  AND w.channel = t.channel';
   ELSE
      clause := '  AND w.channel LIKE t.channel ';
END CASE;
return clause;
END; $$
LANGUAGE 'plpgsql';
CREATE OR REPLACE function public.updateafter_clause() RETURNS TEXT AS $$
-- used to format and optimize a ws station-request
DECLARE
clause TEXT;
BEGIN
clause := '';
SELECT ' AND w.updated >  t.updatedafter '
from temp_req
where temp_req.textdate7 != '*'  INTO clause;
return clause;
END; $$
LANGUAGE 'plpgsql';


CREATE OR REPLACE function public.mtc_clause() RETURNS TEXT AS $$
-- used to format and optimize a ws station-request
DECLARE
clause TEXT;
BEGIN
clause := '';
SELECT '  AND w.mtc = TRUE '
from temp_req
where temp_req.matchtimeseries = TRUE and temp_req.level IN ('channel', 'response') INTO clause;
SELECT '  AND w.mts = TRUE '
from temp_req
where temp_req.matchtimeseries = TRUE and temp_req.level IN ('network', 'station') INTO clause;
return clause;
END; $$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION public."distance_epicentrale_deg"(DOUBLE PRECISION, DOUBLE PRECISION, DOUBLE PRECISION, DOUBLE PRECISION) RETURNS DOUBLE PRECISION AS '
   DECLARE
      p_lati ALIAS FOR $1;            -- Latitude du point
      p_long ALIAS FOR $2;            -- Longitude du point
      s_lati ALIAS FOR $3;            -- Latitude de la station
      s_long ALIAS FOR $4;            -- Longitude de la station
      ss_dist FLOAT8;                  -- Distance sur le grand cercle en degres, default 0
   BEGIN
      ss_dist := 0;
      ss_dist := (180/pi())*            -- Mise en Degre a partir des radians
            (acos(cos(
            atan(tan(radians(p_lati))*0.993305)   -- L1
            )*cos(
            atan(tan(radians(s_lati))*0.993305)   -- L2
            )*cos(
            radians(p_long) - radians(s_long)   -- D1
            )+sin(
            atan(tan(radians(p_lati))*0.993305)   -- L1
            )*sin(
            atan(tan(radians(s_lati))*0.993305)   -- L2
            )));
      RETURN ss_dist ;
   END;'
LANGUAGE 'plpgsql';
