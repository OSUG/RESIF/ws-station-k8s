#!/usr/bin/env bash
#
# Ce script charge les tables nécessaire au bon fonctionnement du webservice station
#
export PGPASSWORD=${POSTGRES_PASSWORD}
export PGUSER=${POSTGRES_USER}

psql_src="psql -h $SRC_PGHOST -d $SRC_PGDATABASE"
psql_dest="psql -d $POSTGRES_DB"

# Chargement du schéma
for table in ws_common ws_network_text ws_station_text ws_channel_text ws_network_xml ws_station_xml ws_channel_xml networks response; do
    $psql_src -c "copy (select * from $table) to stdout" | $psql_dest -c "copy $table from stdin"
done
